//
//  AddContactView.swift
//  CoreDataStack
//
//  Created by rafiul hasan on 12/12/21.
//

import SwiftUI

struct AddContactView: View {
    @ObservedObject var contactVM = CoreDataManager()
    @Environment(\.presentationMode) var presentationMode
    @State private var firstName: String = ""
    @State private var lastName: String = ""
    @State private var phoneNumber: String = ""
    
    var body: some View {
        NavigationView {
            VStack {
                TextField("First Name", text: $firstName)
                    .textFieldStyle(RoundedBorderTextFieldStyle.roundedBorder)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                
                TextField("Last Name", text: $lastName)
                    .textFieldStyle(RoundedBorderTextFieldStyle.roundedBorder)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                
                TextField("Phone Number", text: $phoneNumber)
                    .textFieldStyle(RoundedBorderTextFieldStyle.roundedBorder)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                
                Button(action: {
                    contactVM.saveContact(firstName: firstName, lastName: lastName, phoneNumber: phoneNumber)
                }) {
                    Text("Save")
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .frame(width: UIScreen.main.bounds.width - 40, height: 44)
                        .background(Color.blue)
                        .cornerRadius(12)
                }
            }
            .padding()
            .navigationBarItems(trailing: Button(action: {
                presentationMode.wrappedValue.dismiss()
            }) {
                Image(systemName: "xmark.circle")
            })
        }
    }
}

struct AddContactView_Previews: PreviewProvider {
    static var previews: some View {
        AddContactView()
    }
}
