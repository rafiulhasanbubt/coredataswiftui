//
//  ContentView.swift
//  CoreDataStack
//
//  Created by rafiul hasan on 12/12/21.
//

import SwiftUI

struct ContentView: View {
    let coreDM: CoreDataManager
    @State private var firstName: String = ""
    @State private var lastName: String = ""
    @State private var phoneNumber: String = ""
    
    @State private var contacts: [Contact] = [Contact]()
    @State private var needsRefresh: Bool = false
    @State private var showingSheet = false
    
    private func populateContacts() {
        contacts = coreDM.getAllContact()
    }
    
    var body: some View {
        NavigationView {
            List {
                ForEach(contacts, id: \.self) { contact in
                    NavigationLink(
                        destination: ContactDetailView(coreDM: coreDM, contact: contact, needsRefresh: $needsRefresh),
                        label: {
                            ContactView(contact: contact)
                        })
                }
                .onDelete(perform: { indexSet in
                    indexSet.forEach { index in
                        let contact = contacts[index]
                        coreDM.deleteContact(contact: contact)
                        populateContacts()
                    }
                })
            }
            .listStyle(.plain)
            .navigationTitle("Contacts")
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(trailing: Button(action: {
                showingSheet.toggle()
            }, label: {
                Image(systemName: "plus")
            }))
            .sheet(isPresented: $showingSheet) { AddContactView() }
            .onAppear(perform: { populateContacts() })
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(coreDM: CoreDataManager())
    }
}
