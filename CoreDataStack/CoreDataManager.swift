//
//  CoreDataManager.swift
//  CoreDataStack
//
//  Created by rafiul hasan on 12/12/21.
//

import Foundation
import CoreData

class CoreDataManager: ObservableObject {
    let persistentContainer: NSPersistentContainer
    init() {
        persistentContainer = NSPersistentContainer(name: "ContactsModel")
        persistentContainer.loadPersistentStores { (description, error) in
            if let error = error {
                fatalError("Core Data Store failed \(error.localizedDescription)")
            }
        }
    }
    
    func updateContact() {
        do {
            try persistentContainer.viewContext.save()
        } catch {
            persistentContainer.viewContext.rollback()
        }
    }
    
    func deleteContact(contact: Contact) {
        persistentContainer.viewContext.delete(contact)
        do {
            try persistentContainer.viewContext.save()
        } catch {
            persistentContainer.viewContext.rollback()
            print("Failed to save context \(error)")
        }
    }
    
    func getAllContact() -> [Contact] {
        let fetchRequest: NSFetchRequest<Contact> = Contact.fetchRequest()
        do {
            return try persistentContainer.viewContext.fetch(fetchRequest)
        } catch {
            return []
        }
    }
    
    func saveContact(firstName: String, lastName: String, phoneNumber: String) {
        let contact = Contact(context: persistentContainer.viewContext)
        contact.firstName = firstName
        contact.lastName = lastName
        contact.phoneNumber = phoneNumber
        
        do {
            try persistentContainer.viewContext.save()
        } catch {
            print("Failed to save contact \(error)")
        }
    }
}

