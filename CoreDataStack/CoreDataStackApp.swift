//
//  CoreDataStackApp.swift
//  CoreDataStack
//
//  Created by rafiul hasan on 12/12/21.
//

import SwiftUI

@main
struct CoreDataStackApp: App {
    
    var body: some Scene {
        WindowGroup {
            ContentView(coreDM: CoreDataManager())
        }
    }
}
