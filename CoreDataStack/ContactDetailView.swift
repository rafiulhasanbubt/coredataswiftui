//
//  ContactDetailView.swift
//  CoreDataStack
//
//  Created by rafiul hasan on 12/12/21.
//

import SwiftUI

struct ContactDetailView: View {
    let coreDM: CoreDataManager
    let contact: Contact
    @State private var firstName: String = ""
    @State private var lastName: String = ""
    @State private var phoneNumber: String = ""
    @Binding var needsRefresh: Bool
    
    var body: some View {
        Spacer()
        VStack {
            Image(systemName: "person")
                .resizable()
                .frame(width: 160, height: 160)
                .scaledToFit()
                .clipShape(Circle())
                .padding(.bottom, 40)
            
            TextField(contact.firstName ?? "", text: $firstName)
                .textFieldStyle(RoundedBorderTextFieldStyle())
            
            TextField(contact.lastName ?? "", text: $lastName)
                .textFieldStyle(RoundedBorderTextFieldStyle())
            
            TextField(contact.phoneNumber ?? "", text: $phoneNumber)
                .textFieldStyle(RoundedBorderTextFieldStyle())
            
            Button("Update") {
                if !firstName.isEmpty {
                    contact.firstName = firstName
                    contact.lastName = lastName
                    contact.phoneNumber = phoneNumber
                    
                    coreDM.updateContact()
                    needsRefresh.toggle()
                }
            }
            .foregroundColor(.white)
            .frame(width: UIScreen.main.bounds.width - 40, height: 44)
            .background(Color.blue)
            .cornerRadius(12).padding(.top, 40)
        }
        .padding()
        
        Spacer()
    }
}

struct ContactDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ContactDetailView(coreDM: CoreDataManager(), contact: Contact(), needsRefresh: .constant(true))
    }
}
