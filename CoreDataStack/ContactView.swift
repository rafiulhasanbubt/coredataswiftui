//
//  ContactView.swift
//  CoreDataStack
//
//  Created by rafiul hasan on 12/12/21.
//

import SwiftUI

struct ContactView: View {
    let contact: Contact
    
    var body: some View {
        HStack {
            Image(systemName: "person")
                .resizable()
                .frame(width: 40, height: 40)
                .clipShape(Circle())
            
            Text(contact.firstName ?? "-")
            Text(contact.lastName ?? "-")
            Spacer()
            Text(contact.phoneNumber ?? "-")
        }
    }
}

struct ContactView_Previews: PreviewProvider {
    static var previews: some View {
        ContactView(contact: Contact())
    }
}
